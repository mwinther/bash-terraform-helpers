#!/bin/bash

ACCESS_TOKEN=$(
curl -s --location "https://auth.idp.hashicorp.com/oauth2/token" \
     --header "Content-Type: application/x-www-form-urlencoded" \
     --data-urlencode "client_id=$HCP_CLIENT_ID" \
     --data-urlencode "client_secret=$HCP_CLIENT_SECRET" \
     --data-urlencode "grant_type=client_credentials" \
     --data-urlencode "audience=https://api.hashicorp.cloud" \
     | jq -r .access_token)
if [ 0 -eq $? ]; then
  ORG_ID=$(
    curl -s --header "Authorization: Bearer $ACCESS_TOKEN" \
    https://api.cloud.hashicorp.com/resource-manager/2019-12-10/organizations \
    | jq -r ".organizations[0].id")
  echo export HCP_API_TOKEN=$ACCESS_TOKEN
  echo export HCP_ORGANIZATION_ID=$ORG_ID
fi
